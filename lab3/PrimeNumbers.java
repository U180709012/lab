public class PrimeNumbers {
    public static void main(String[] args) {
        //Get the number
        int given = Integer.parseInt(args[0]);
        //For each number less than the given
        for (int number = 2; number < given; number++) {
            boolean isPrime = true;

            for (int divisor = 2; divisor < number; divisor++) {

                if (number % divisor == 0) {

                    isPrime = false;

                    break;

                }
            }

                if (isPrime)
                    System.out.println(number + ", ");

            }

        }
    }

