 //
//  code recreated from a .class file by IntelliJ IDEA
// (powered by FeSourcernflower decompiler)
//

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class MultipleGuess2 {
    public MultipleGuess2() {
    }

    public static void main(String[] var0) throws IOException {

        Scanner var1 = new Scanner(System.in);
        Random var2 = new Random();
        int var3 = var2.nextInt(100);

        System.out.println("Hi! I'm thinking of a number between 0 and 99.");
        System.out.print("Can you guess it: ");
        int how=0;
        int var4;

        do {
            var4 = var1.nextInt();
            how+=1;

            if (var3==var4) {
                System.out.println("Congratulations");
            }
            else if(var4 == -1){
                System.out.println("Sorry the number was: "+var3);}
            else{
                System.out.println("Sorry " +
                        "Type -1 to quit or guess another");
            }


        }while (var4 !=var3);
        System.out.println("You won after " + how +" attempts!");

        var1.close();

    }
}

	
	